<?php

//namespace Tests\Unit;
require 'Login.php';
//use PHPUNIT_Framework_TestCase as TestCase;
use PHPUnit\Framework\TestCase;
//use Login;
class PasswordLengthTest extends TestCase
{
 
    
    public function testLength() {
        $log = new Login('','');
        // $pass= '12345678';
        //$result = $log->checkPassLength($pass);
        $this->assertTrue( $log->checkPassLength('12345678')); //jei daugiau nei 8 true
        $this->assertFalse( $log->checkPassLength('1234567')); //jei maziau nei 8 false
    }
    public function testSpecialChar() {
        $log = new Login('','');
        // $pass= '12345678';
        //$result = $log->checkPassLength($pass);
        $this->assertTrue( $log->checkIsSpecialChar('123@5678')); //jei daugiau nei 1 special char true
        $this->assertFalse( $log->checkIsSpecialChar('1234567')); //jei maziau nei 1 special char false
    }
    public function testCheckOneNumber() {
        $log = new Login('','');
        // $pass= '12345678';
        //$result = $log->checkPassLength($pass);
        $this->assertTrue( $log->checkIfContainsNumeric('abc1')); //jei daugiau nei 1 skaicius true
        $this->assertFalse( $log->checkIfContainsNumeric('abcd')); //jei maziau nei 1 skaicius false
    }




}

